import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './guards/auth-guard.service';
import { ProfileComponent } from './components/profile/profile.component';
import { LoginComponent } from './components/login/login.component';
import { FriendsComponent } from './components/friends/friends.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { GamesComponent } from './components/games/games.component';

const routes: Routes = [
  // { path: '', redirectTo: 'profile' },
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'games', component: GamesComponent, pathMatch: 'full' },
  // { path: 'library', component: HomeComponent, pathMatch: 'full' },
  { path: 'friends', component: FriendsComponent, pathMatch: 'full' },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
