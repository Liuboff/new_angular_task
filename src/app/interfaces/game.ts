export interface IGame {
    id: number | string;
    name: string;
    description: string;
    price: string;
    tags: Array<string>;
}
