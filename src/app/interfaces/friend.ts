export interface IFriend {
    id: number | string;
    name: string;
    email: string;
  }
  