import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Store, select } from '@ngrx/store';

import { UpdateUser } from 'src/app/store/actions/user.action';
import { selectUserList, selectSelectedUser } from 'src/app/store/selectors/user.selectors';
import { AuthService } from '../../services/auth.service';
import { IUser } from 'src/app/interfaces/user';
import { IAppState } from 'src/app/store/state';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    private authService: AuthService,
    private store: Store<IAppState>
  ) { }

  myUser!: IUser;

  profileForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    age: ['', [Validators.required, Validators.min(1), Validators.max(120)]],
  });

  ngOnInit(): void {
    this.store.select(selectSelectedUser).subscribe(user => this.myUser = user); //why is empty ????
    console.log(this.myUser);
    // this.profileForm.patchValue({ email: this.myUser.email });
  }

  saveProfile() {
    this.store.dispatch(new UpdateUser(this.profileForm.value));
  }

}
