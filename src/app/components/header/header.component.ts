import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { CookiesService } from '../../services/cookies.service'
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showNav: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private cookieService: CookiesService,
    private localStorageService: LocalStorageService) { 
      this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          this.showNav = this.localStorageService.getData('isLoggedIn') === 'logged';
        }
      })
     }

  ngOnInit(): void {}

  logout() {
    this.authService.logout();
  }
  
}
