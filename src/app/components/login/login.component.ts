import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, pipe, find } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { IUser } from 'src/app/interfaces/user';
import { IAppState } from 'src/app/store/state';
import { selectUserList, selectSelectedUser } from 'src/app/store/selectors/user.selectors';
import { GetAllUsers, GetUser } from 'src/app/store/actions/user.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private store: Store<IAppState>) { }

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  });

  users$ = this.store.pipe(select(selectUserList))

  ngOnInit(): void { 
    this.store.dispatch(new GetAllUsers());
  }

  login() {
    if (this.loginForm.valid) {
      this.store.dispatch(new GetUser(this.loginForm.value));
    }
  }

}
