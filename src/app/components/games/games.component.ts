import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {}

  searchGames() {}
}




















// import { Component, OnInit } from '@angular/core';
// import { Validators, FormBuilder } from '@angular/forms';
// import { Router } from '@angular/router';
// import { HttpClient } from '@angular/common/http';
// import { Store, select } from '@ngrx/store';

// import { IGameState } from 'src/app/store/state/game.state';
// import { GetAllGames } from 'src/app/store/actions/game.action';
// import { selectGameList } from 'src/app/store/selectors/game.selectors';
// import { AuthService } from '../../services/auth.service';
// import { IGame } from 'src/app/interfaces/game';
// import { IAppState } from 'src/app/store/state';

// @Component({
//   selector: 'app-games',
//   templateUrl: './games.component.html',
//   styleUrls: ['./games.component.scss']
// })
// export class GamesComponent implements OnInit {

//   constructor(
//     private router: Router,
//     private http: HttpClient,
//     private fb: FormBuilder,
//     private authService: AuthService,
//     private store: Store<IAppState>
//   ) { }

//   games$ = this.store.pipe(select(selectGameList));
//   // myGames: Game[] = [];


//   searchGame!: IGame[];
//   // user!: User;

//   ngOnInit(): void {
//     this.store.dispatch(new GetAllGames());
//   }


//   searchGames(value: string): void {
//     // this.searchGame = games$.filter((item) => item.name.includes(value) && item.name !== this.user.email);
//   }

// }


// // export class FriendsComponent implements OnInit {
// //   searchFriend!: Friend[];
// //   myFriends: Friend[] = [];
// //   user!: User;

// //   constructor(private store: Store<AppState>, private _snackBar: MatSnackBar) {}

// //   ngOnInit(): void {
// //     this.store.select(selectUser).subscribe((users) => {
// //       this.user = users || {};
// //     });
// //     this.store.select(selectFriends).subscribe((friends) => {
// //       this.myFriends = friends || [];
// //     });
// //   }

// //   openSnackBar(message: string, action: string) {
// //     this._snackBar.open(message, action, {
// //       duration: 2000,
// //     });
// //   }

// //   addFriend(friend: Friend): void {
// //     this.store.dispatch(addFriend({ friend: { ...friend } }));
// //   }

// //   removeFriend(friend: Friend): void {
// //     this.store.dispatch(removeFriend({ friend: { ...friend } }));
// //   }

// //   searchFriends(value: string): void {
// //     this.searchFriend = users.filter((item) => item.name.includes(value) && item.email !== this.user.email);
// //   }

// //   ifFriend(user: string): boolean {
// //     return !!this.myFriends.find((item) => item.name === user);
// //   }
// // }
