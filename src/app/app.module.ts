import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
// import { storageSyncMetaReducer } from 'ngrx-store-persist';

import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './guards/auth-guard.service';

import { appReducers } from './store/reducers/index';
import { UsersEffects } from './store/effects/user.effects';
// import { FriendsEffects }from './store/effects/friend.effects';
// import { GamesEffects } from './store/effects/game.effects';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LoginComponent } from './components/login/login.component';
import { FriendsComponent } from './components/friends/friends.component';
import { FriendsListComponent } from './components/friends-list/friends-list.component';
import { FriendsSearchComponent } from './components/friends-search/friends-search.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { GamesComponent } from './components/games/games.component';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProfileComponent,
    LoginComponent,
    FriendsComponent,
    FriendsListComponent,
    FriendsSearchComponent,
    NotFoundPageComponent,
    GamesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    StoreModule.forRoot(appReducers),
    // StoreModule.forRoot(appReducers, { metaReducers: [storageSyncMetaReducer] }),
    EffectsModule.forRoot([UsersEffects]),
    // EffectsModule.forRoot([UsersEffects, FriendsEffects, GamesEffects]),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router'}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    // !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    AuthService,
    AuthGuardService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
