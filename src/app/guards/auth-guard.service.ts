import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from "rxjs";

import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private localStorageService: LocalStorageService, 
    public router: Router) { }

    canActivate(): Observable<boolean> | boolean {
      if (this.localStorageService.getData('isLoggedIn') !== 'logged') {
        this.router.navigate(['']);
        return false;
      }
      return true;
    }
}
