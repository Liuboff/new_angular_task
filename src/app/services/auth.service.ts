import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { CookiesService } from './cookies.service';
import { LocalStorageService } from './local-storage.service';

import { IUser } from '../interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private http: HttpClient,
        private router: Router,
        private cookieService: CookiesService,
        private localStorageService: LocalStorageService) { }

    private usersUrl = `${environment.baseUrl}/users`;

    login(user: IUser) {
        let myUser = this.http.get<IUser>(this.usersUrl, {params: new HttpParams().set('email', user.email)});
        // let myUser = this.http.get<IUser>(`${this.usersUrl}?email=${user.email}`);
        this.localStorageService.setData('isLoggedIn', 'logged');
        this.cookieService.setData('email', user.email);
        this.router.navigate(['profile']);

        return myUser;
    }

    logout() {
        this.localStorageService.setData('isLoggedIn', 'logout');
        this.cookieService.setData('email', '');
        this.router.navigate(['']);
    }

}












// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { Router } from '@angular/router';
// import { environment } from 'src/environments/environment';
// import { CookiesService } from './cookies.service'; 
// import { LocalStorageService } from './local-storage.service';

// import { IUser } from '../interfaces/user';
// import { createFind } from 'rxjs/internal/operators/find';
// import { Observable, of } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthService {

//   constructor(
//     private http: HttpClient,
//     private router: Router,
//     private cookieService: CookiesService,
//     private localStorageService: LocalStorageService) { }

//   private usersUrl = `${environment.baseUrl}/users`;

//   login(user: IUser) {
//     // return this.http.get<IUser[]>(this.usersUrl)
//     // .subscribe((data) => {
//       console.log(user.email);
//       this.localStorageService.setData('isLoggedIn', 'logged');
//       this.cookieService.setData('email', user.email);
//       // this.router.navigate(['profile']);
//     // })

//     // let userSelectedId = this.http.get<IUser[]>(this.usersUrl).subscribe(data => {
//     //   data.find(us => us.email === user.email)?.id;
//     // });

//     // let id = user.id;

//     // console.log(user);
//     // console.log(userSelectedId);

//     // return this.http.get<IUser[]>(this.usersUrl)
//     // return userSelected;

//     // return this.http.get<IUser>(`${this.usersUrl}/${userSelectedId}`);
//     // return this.http.get<IUser>(`${this.usersUrl}/?email=${user.email}`);
//     return this.http.get<IUser>(`${this.usersUrl}/6`);

//     // return of(user);
//   }

//   isLoggedIn(): boolean {
//     return this.localStorageService.getData('isLoggedIn') === 'logged' ? true : false;
//   }

// }