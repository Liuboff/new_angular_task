import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private usersUrl = `${environment.baseUrl}/users`;

  getAllUsers() {
    return this.http.get<IUser[]>(this.usersUrl);
  }
  
  getUser(email: string) {
    return this.http.get<IUser>(`${this.usersUrl}?email=${email}`);
  }

  updateUser(user: IUser) {
    return this.http.put<IUser>(this.usersUrl, user);
  }

}