import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {

  constructor(private http: HttpClient) { }

  private usersUrl = `${environment.baseUrl}/users`;

  getAllFriends() {
    return this.http.get<any>(this.usersUrl);
  }

  // getFriends(): Observable<User[]> {
  //   return this.http.get<User[]>(this.usersUrl);
  // }

}
