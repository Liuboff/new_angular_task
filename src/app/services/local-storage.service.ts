import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setData (name: string, data: string): void {
    localStorage.setItem(name, data);
  }

  getData (name: string): string | null {
    return localStorage.getItem(name);
  }

}
