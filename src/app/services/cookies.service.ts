import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CookiesService {

  constructor( private cookieService: CookieService ) {}

  setData (name: string, data: string): void {
    this.cookieService.set(name, data);
  }

  getData (name: string): string {
    return this.cookieService.get(name);
  }

}
