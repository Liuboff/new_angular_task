import { GamesUnion, EGamesActions } from '../actions/game.action';
import { IGameState, initialIGameState } from '../state/game.state';


export const gameReducer = (
    state: IGameState = initialIGameState,
    action: GamesUnion
): IGameState => {
    switch (action.type) {

        case EGamesActions.GamesSuccess: {
            console.log(action);
            console.log(state);
            return {
                ...state,
                games: [...state.games]
            };
        }
        case EGamesActions.GamesFailure: {
            return {
                ...state,
                games: [...state.games]
            };
        }

        default:
            return state;
    }
}























// import { GamesUnion, EGamesActions } from '../actions/game.action';
// import { IGameState, initialIGameState } from '../state/game.state';


// export const gameReducer = (
//     state: IGameState = initialIGameState,
//     action: GamesUnion
// ): IGameState => {
//     switch (action.type) {

//         //     case GamesActions.GetAllGames:
//         //       return {
//         //         ...state,
//         //         games: [...state.games]
//         //       };
//         //    case GamesActions.GetGame:
//         //       return {
//         //         ...state,
//         //         games: [...state.games]
//         //       };
//         //     case GamesActions.CreateGame:
//         //       return {
//         //         ...state,
//         //         games: [...state.games]
//         //       };
//         //     case GamesActions.UpdateGame:
//         //       return {
//         //         ...state,
//         //         games: [...state.games]
//         //       };
//         //     case GamesActions.DeleteGame:
//         //       return {
//         //         ...state,
//         //         games: [...state.games]
//         //       };
//         case EGamesActions.GamesSuccess: {
//             console.log(action);
//             console.log(state);
//             return {
//                 ...state,
//                 games: [...state.games]
//             };
//         }
//         case EGamesActions.GamesFailure: {
//             return {
//                 ...state,
//                 games: [...state.games]
//             };
//         }

//         default:
//             return state;
//     }
// }
