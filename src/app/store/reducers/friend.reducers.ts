import { FriendsUnion, EFriendsActions } from '../actions/friend.action';
import { IFriendState, initialIFriendState } from '../state/friend.state';

export const friendReducer = (
    state: IFriendState = initialIFriendState,
    action: FriendsUnion
): IFriendState => {

    switch (action.type) {
        case EFriendsActions.FriendsSuccess: {
            console.log(action.payload);
            return {
                ...state,
                friends: action.payload
            };
        }
        case EFriendsActions.GetFriendSuccess: {
            console.log(action.payload);
            console.log(state);
            return {
                ...state,
                friend: action.payload
            };
        }

        default:
            return state;
    }
}













// // import { userInfo } from 'os';
// import { FriendsUnion, FriendsActions } from '../actions/friend.action';
// import { IFriendState, initialIFriendState } from '../state/friend.state';
// import { IUserState, initialIUserState } from "../state/user.state";

// // export function friendReducer(state: IFriendState = initialIFriendState, userState: IUserState = initialIUserState, action: FriendsUnion) {
// export function friendReducer(state: IFriendState = initialIFriendState, action: FriendsUnion) {
//   switch (action.type) {
//     case FriendsActions.GetAllFriends:
//       // const friendsIds = userState.selectedUser.friends;

//       // const friendsIds = action.payload.friendsIds;
//       // console.log(action.payload.friendsIds);

//       // const friends = (friendsIds?.map(id => { userState.users[id].id, userState.users[id].name, userState.users[id].email }));
//       return {
//         ...state,
//         friends: [...state.friends]
//       };
//     case FriendsActions.GetFriend:
//       return {
//         ...state,
//         friends: [...state.friends]
//       };
//     case FriendsActions.AddFriend:
//       const newFriendId = action.payload.id;
//       // const newFriend = userState.users.find((user) => user.id == newFriendId);
//       // const newFriend = userState.users.find((user) => user.id == newFriendId);
//       return {
//         ...state,
//         friends: [...state.friends]
//         // friends: [...state.friends, newFriend]
//       };
//     case FriendsActions.DeleteFriend:
//       return {
//         ...state,
//         friends: [...state.friends]
//       };
//     case FriendsActions.FriendsSuccess:
//       console.log(action);
//       console.log(state);
//       return {
//         ...state,
//         friends: [...state.friends]
//       };
//     case FriendsActions.FriendsFailure:
//       return {
//         ...state,
//         friends: [...state.friends]
//       };
//     default:
//       return state;
//   }
// }
