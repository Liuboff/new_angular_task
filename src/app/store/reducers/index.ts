import { ActionReducerMap } from "@ngrx/store";
import { routerReducer } from "@ngrx/router-store";
import { IAppState } from "../state";

import { gameReducer } from "./game.reducers";
import { friendReducer } from "./friend.reducers";
import { userReducer } from "./user.reducers";

export const appReducers: ActionReducerMap<IAppState, any> = {
    router: routerReducer,
    users: userReducer,
    friends: friendReducer,
    games: gameReducer
}