import { act } from '@ngrx/effects';
import { UsersActions, EUsersActions } from '../actions/user.action';
import { IUserState, initialIUserState } from '../state/user.state';

export const userReducer = (
    state: IUserState = initialIUserState,
    action: UsersActions
): IUserState => {

    switch (action.type) {
        case EUsersActions.GetAllUsers: {
            return {
                ...state,
                users: [...state.users]
            };
        }
        case EUsersActions.UsersSuccess: {
            console.log(action.payload);
            return {
                ...state,
                users: action.payload
            };
        }
        case EUsersActions.UsersFailure: {
            console.log(action.payload);
            return {
                ...state
            };
        }
        case EUsersActions.GetUser: {
            console.log(action.payload);
            return {
                ...state
            };
        }
        case EUsersActions.GetUserSuccess: {
            console.log(action.payload);
            return {
                ...state,
                selectedUser: action.payload
            };
        }
        case EUsersActions.GetUserFailure: {
            console.log(action.payload);
            return {
                ...state
            };
        }

        case EUsersActions.UpdateUser: {
            console.log(action.payload);
            return {
                ...state
            };
        }
        case EUsersActions.UpdateUserSuccess: {
            console.log(action.payload);
            console.log(state);
            return {
                ...state,
                selectedUser: action.payload
            };
        }
        case EUsersActions.UpdateUserFailure: {
            console.log(action.payload);
            return {
                ...state
            };
        }

        default:
            return state;
    }
}
