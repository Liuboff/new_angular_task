import { Action } from '@ngrx/store';

import { IUser } from 'src/app/interfaces/user';

export enum EUsersActions {
    GetAllUsers = '[Users] GetAllUsers',
    UsersSuccess = '[Users] UsersSuccess',
    UsersFailure = '[Users] UsersFailure',
    GetUser = '[Users] GetUser',
    GetUserSuccess = '[Users] GetUserSuccess',
    GetUserFailure = '[Users] GetUserFailure',
    UpdateUser = '[Users] UpdateUser',
    UpdateUserSuccess = '[Users] UpdateUserSuccess',
    UpdateUserFailure = '[Users] UpdateUserFailure',

}

export class GetAllUsers implements Action {
    readonly type = EUsersActions.GetAllUsers;
}

export class UsersSuccess implements Action {
    readonly type = EUsersActions.UsersSuccess;
    constructor(public payload: IUser[]) { }
}

export class UsersFailure implements Action {
    readonly type = EUsersActions.UsersFailure;
    constructor(public payload: Error) { }
}

export class GetUser implements Action {
    readonly type = EUsersActions.GetUser;
    constructor(public payload: IUser ) { }
}

export class GetUserSuccess implements Action {
    readonly type = EUsersActions.GetUserSuccess;
    constructor(public payload: IUser) { }
}

export class GetUserFailure implements Action {
    readonly type = EUsersActions.GetUserFailure;
    constructor(public payload: Error) { }
}

export class UpdateUser implements Action {
    readonly type = EUsersActions.UpdateUser;

    constructor(public payload: IUser) { }
}

export class UpdateUserSuccess implements Action {
    readonly type = EUsersActions.UpdateUserSuccess;
    constructor(public payload: IUser) { }
}

export class UpdateUserFailure implements Action {
    readonly type = EUsersActions.UpdateUserFailure;
    constructor(public payload: Error) { }
}

export type UsersActions =
    GetAllUsers |
    UsersSuccess |
    UsersFailure |
    GetUser |
    GetUserSuccess |
    GetUserFailure |
    UpdateUser |
    UpdateUserSuccess |
    UpdateUserFailure;






















// // import { createAction, props } from '@ngrx/store';
// // import { IUser } from 'src/app/interfaces/user';

// // export const getAllUsers = createAction('[Users] GetAllUsers');

// // export const login = createAction('[Login Page] Login',
// //   props<{ user: IUser }>()
// // );

// // export const usersSuccess = createAction('[Users] UsersSuccess',
// //   props<{ users: IUser[] }>()
// // );

// // export const updateUser = createAction('[USER] Update User', props<{ user: User }>());

// // export const addFriend = createAction('[USER] Add Friend', props<{ friend: Friend }>());

// // export const removeFriend = createAction('[USER] Remove Friend', props<{ friend: Friend }>());

// // export const addGame = createAction('[USER] Add Game', props<{ game: Game }>());


// // export const login = createAction(
// //   '[Login Page] Login',
// //   props<{ username: string; password: string }>()
// // );









// import { Action } from '@ngrx/store';
// import { IUser } from 'src/app/interfaces/user';

// export enum EUsersActions {
//   GetAllUsers = '[Users] GetAllUsers',
//   GetUser = '[Users] GetUser',
//   CreateUser = '[Users] CreateUser',
//   UpdateUser = '[Users] UpdateUser',
//   DeleteUser = '[Users] DeleteUser',
//   UsersSuccess = '[Users] UsersSuccess',
//   GetUserSuccess = '[Users] GetUserSuccess',
//   UsersFailure = '[Users] UsersFailure'
// }

// export class GetAllUsers implements Action {
//   readonly type = EUsersActions.GetAllUsers;
//   // constructor(public payload: any) { }
// }

// export class GetUser implements Action {
//   readonly type = EUsersActions.GetUser;

//   // constructor(public payload: { user: IUser }) { }
//   constructor(public payload: { email: string }) { }
// }

// export class GetUserSuccess implements Action {
//   readonly type = EUsersActions.GetUserSuccess;

//   constructor(public payload: IUser ) { }
// }

// export class CreateUser implements Action {
//   readonly type = EUsersActions.CreateUser;

//   constructor(public payload: { user: IUser }) { }
// }

// export class UpdateUser implements Action {
//   readonly type = EUsersActions.UpdateUser;

//   constructor(public payload: { user: IUser }) { }
// }

// export class DeleteUser implements Action {
//   readonly type = EUsersActions.DeleteUser;

//   constructor(public payload: { id: number }) { }
// }

// export class UsersSuccess implements Action {
//   readonly type = EUsersActions.UsersSuccess;

//   constructor(public payload: any ) { }
// }

// export class UsersFailure implements Action {
//   readonly type = EUsersActions.UsersFailure;

//   constructor(public payload: { error: any }) { }
// }

// export type UsersActions = GetAllUsers | GetUser | GetUserSuccess | CreateUser | UpdateUser | DeleteUser | UsersSuccess | UsersFailure;
