import { Action } from '@ngrx/store';
import { IGame } from '../../interfaces/game';

export enum EGamesActions {
  GetAllGames = '[Games] getAllGames',
  GetGame = '[Games] GetGame',
  CreateGame = '[Games] CreateGame',
  UpdateGame = '[Games] UpdateGame',
  DeleteGame = '[Games] DeleteGame',
  GamesSuccess = '[Games] GamesSuccess',
  GamesFailure = '[Games] GamesFailure'
}

export class GetAllGames implements Action {
  readonly type = EGamesActions.GetAllGames;
}

export class GamesSuccess implements Action {
    readonly type = EGamesActions.GamesSuccess;
  
    constructor(public payload: IGame[]) {}
}

export class GamesFailure implements Action {
    readonly type = EGamesActions.GamesFailure;
  
    constructor(public payload: Error) {}
}

export class GetGame implements Action {
  readonly type = EGamesActions.GetGame;

  constructor(public payload: { id: string }) {}
}

export class CreateGame implements Action {
  readonly type = EGamesActions.CreateGame;

  constructor(public payload: { game: IGame }) {}
}

export class UpdateGame implements Action {
  readonly type = EGamesActions.UpdateGame;

  constructor(public payload: { game: IGame }) {}
}

export class DeleteGame implements Action {
  readonly type = EGamesActions.DeleteGame;

  constructor(public payload: { id: number }) {}
}



export type GamesUnion = 
GetAllGames | 
GamesSuccess | 
GamesFailure |
GetGame | 
CreateGame | 
DeleteGame | 
UpdateGame;
