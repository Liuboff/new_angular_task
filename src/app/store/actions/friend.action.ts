import { Action } from '@ngrx/store';
import { IFriend } from 'src/app/interfaces/friend';

export enum EFriendsActions {
    GetAllFriends = '[Friends] getAllFriends',
    FriendsSuccess = '[Friends] FriendsSuccess',
    FriendsFailure = '[Friends] FriendsFailure',
    GetFriend = '[Friends] GetFriend',
    GetFriendSuccess = '[Friends] GetFriendSuccess',
    GetFriendFailure = '[Friends] GetFriendFailure',
    AddFriend = '[Friends] AddFriend',
    DeleteFriend = '[Friends] DeleteFriend',
}

export class GetAllFriends implements Action {
    readonly type = EFriendsActions.GetAllFriends;
    constructor(public payload: number[]) { }
}

export class FriendsSuccess implements Action {
    readonly type = EFriendsActions.FriendsSuccess;
    constructor(public payload: IFriend[]) { }
}

export class FriendsFailure implements Action {
    readonly type = EFriendsActions.FriendsFailure;
    constructor(public payload: Error) { }
}

export class GetFriend implements Action {
    readonly type = EFriendsActions.GetFriend;
    constructor(public payload: number) { }
}

export class GetFriendSuccess implements Action {
    readonly type = EFriendsActions.GetFriendSuccess;
    constructor(public payload: IFriend) { }
}

export class GetFriendFailure implements Action {
    readonly type = EFriendsActions.GetFriendFailure;
    constructor(public payload: Error) { }
}

export class AddFriend implements Action {
    readonly type = EFriendsActions.AddFriend;
    constructor(public payload: number) { }
}

export class DeleteFriend implements Action {
    readonly type = EFriendsActions.DeleteFriend;
    constructor(public payload: number) { }
}


export type FriendsUnion =
    GetAllFriends |
    FriendsSuccess |
    FriendsFailure |
    GetFriend |
    GetFriendSuccess |
    GetFriendFailure |
    AddFriend |
    DeleteFriend;
