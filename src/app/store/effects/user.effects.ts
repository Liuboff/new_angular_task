import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap, map, mergeMap, tap, catchError } from 'rxjs/operators';

import { IAppState } from '../state/index';
import {
    EUsersActions, UsersSuccess, UsersFailure, GetUser, GetUserSuccess,
    UpdateUser, UpdateUserSuccess, UpdateUserFailure
} from '../actions/user.action';

import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { IUser } from 'src/app/interfaces/user';
import { selectUserList } from '../selectors/user.selectors';

@Injectable({
    providedIn: 'root'
})
export class UsersEffects {
    constructor(
        private userService: UserService,
        private authService: AuthService,
        private actions$: Actions,
        private router: Router,
        private store: Store<IAppState>
    ) { }

    @Effect()
    getUsers$ = this.actions$.pipe(
        ofType(EUsersActions.GetAllUsers),
        mergeMap(() => this.userService.getAllUsers()
            .pipe(
                map((users: IUser[]) => (new UsersSuccess(users))),
                catchError(error => of(new UsersFailure(error)))
            )
        )
    );

    @Effect()
    getUser$ = this.actions$.pipe(
        ofType(EUsersActions.GetUser),
        switchMap((action: GetUser) => this.authService.login(action.payload)
            .pipe(
                map((user: IUser) => (new GetUserSuccess(user))),
                catchError(error => of(new UsersFailure(error)))
            )
        )
    );

    @Effect()
    updateUser$ = this.actions$.pipe(
        ofType(EUsersActions.UpdateUser),
        switchMap((action: UpdateUser) => this.userService.updateUser(action.payload)
            .pipe(
                map((user: IUser) => (new UpdateUserSuccess(user))),
                catchError(error => of(new UpdateUserFailure(error)))
            )
        )
    );

    // @Effect({ dispatch: false })
    // UpdateUserSuccess$ = this.actions$.pipe(
    //     ofType(EUsersActions.UpdateUserSuccess),
    //     tap(() => {
    //         this.router.navigateByUrl('/friends');
    //     })
    // );

}
