// import { Injectable } from '@angular/core';
// import { Effect, ofType, Actions } from '@ngrx/effects';
// import { Store, select } from '@ngrx/store';
// import { of } from 'rxjs';
// import { switchMap } from 'rxjs';

// import { IAppState } from '../state';
// import { GamesActions, GetAllGames, GamesSuccess } from '../actions/game.action';

// import { GamesService } from 'src/app/services/games.service';
// import { selectGameList } from '../selectors/game.selectors';


// @Injectable({
//     providedIn: 'root'
// })
// export class GamesEffects {

//     constructor(
//         private gameService: GamesService,
//         private actions$: Actions,
//         private store: Store<IAppState>
//     ) { }

//     @Effect()
//     getGames$ = this.actions$.pipe(
//         ofType<GetAllGames>(GamesActions.GetAllGames),
//         switchMap(() => this.gameService.getAllGames()),
//         switchMap((data) => of(new GamesSuccess(data)))
//     )

// }
