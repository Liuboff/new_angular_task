import { IUser } from '../../interfaces/user';

export interface IUserState {
    users: IUser[];
    selectedUser: IUser;
}

export const initialIUserState: IUserState = {
    users: [],
    selectedUser: {
        id: '',
        email: '',
        password: '',
        name: '',
        friends: [],
        games: []
    }
};