import { IGame } from '../../interfaces/game';

export interface IGameState {
    games: IGame[],
    game: IGame;
}

export const initialIGameState: IGameState = {
    games: [],
    game: {
        id: '',
        name: '',
        description: '',
        price: '',
        tags: []
    }
};