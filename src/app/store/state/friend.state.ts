import { IFriend } from '../../interfaces/friend';
import { IUserState, initialIUserState } from "./user.state";

export interface IFriendState {
    friends: IFriend[];
    friend: IFriend;
}

export const initialIFriendState: IFriendState = {
    friends: [],
    friend: {
        id: '',
        name: '',
        email: ''
    }
};