import { RouterReducerState } from "@ngrx/router-store";
import { IGameState, initialIGameState } from "./game.state";
import { IFriendState, initialIFriendState } from "./friend.state";
import { IUserState, initialIUserState } from "./user.state";

export interface IAppState {
    router?: RouterReducerState;
    users: IUserState;
    games: IGameState;
    friends: IFriendState;
}

export const initialAppState: IAppState = {
    users: initialIUserState,
    games: initialIGameState,
    friends: initialIFriendState
};

export function getInitialState(): IAppState {
    return initialAppState;
}
