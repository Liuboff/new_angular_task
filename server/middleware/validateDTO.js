function validateDTO(ajv) {
  return (req, res, next) => {
    const valid = ajv(req.body);
    if (!valid) {
      const errors = ajv.errors;
      res.status(400).json(errors);
    }
    next();
  }
}

module.exports = validateDTO;