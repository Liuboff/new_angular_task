const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Game = new Schema({
  name: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  tag: [String],
  price: Number,
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

module.exports = mongoose.model('Game', Game);

