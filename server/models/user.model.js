module.exports = mongoose => {
  const User = mongoose.model(
    'User',
    mongoose.Schema(
      {
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        name: { type: String, required: true },
        age: Number,
        friends: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
        games: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Game' }],
        phone: Number,
        token: String,
        avatar: String
      }
    )
  );
  return User;
};
