const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const config = require('./config/config');
const usersRouter = require('./routes/user.routes.js');
require('./connectionDb/db');

const app = express();
var corsOptions = {
  origin: config.web.origin
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(fileUpload({}));
app.use(express.urlencoded({ extended: true }));

app.use('/api/users', usersRouter);

const PORT = config.web.port;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});