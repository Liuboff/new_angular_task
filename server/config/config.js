// module.exports = {
//   url: 'mongodb://localhost:27017/angular_task'
// };

const config = {
  db: {
    url: 'mongodb://localhost:27017/angular_task'
  },

  web: {
    port: process.env.PORT || 8080,
    origin: 'http://localhost:8081'
  }
};

module.exports = config;