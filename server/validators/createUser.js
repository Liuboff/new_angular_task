const ajv = require('./ajv');

let userSchema = {
  type: 'object',
  properties: {
    email: {type: 'string', format: 'email',},
    password: {type: 'string'},
    name: {type: 'string'},
    age: {type: 'number'},
    phone: {type: 'number'},
    token: {type: 'string'},
  },
  required: ['email', 'password', 'name'],
  additionalProperties: true
}

let validateUser = ajv.compile(userSchema);

module.exports = validateUser;