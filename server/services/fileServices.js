const uuid = require('uuid');
const path = require('path');

class FileService {
  saveFile(file) {
    try {
      if (file.mimetype === 'image/bmp'  || 
          file.mimetype === 'image/gif'  || 
          file.mimetype === 'image/jpeg' || 
          file.mimetype === 'image/png'  || 
          file.mimetype === 'image/webp') {
        const extension = file.mimetype.split('/')[1];
        const fileName = `${uuid.v4()}.${extension}`;
        const filePath = path.resolve('static', fileName);
        file.mv(filePath);
        return fileName;
      }
      // else{
      //   throw new Error('Inappropriate extension of image');
      // }
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = new FileService();