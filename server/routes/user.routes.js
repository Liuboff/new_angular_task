const users = require('../controllers/user.controller.js');
const router = require('express').Router();
const userValidator = require('../middleware/validateDTO');
const userSchema = require('../validators/createUser');

router.post('/', userValidator(userSchema), (req, res, next) => {
  users.create(req.body, req.files)
  .then((answer) => {
    res.json(answer);
  })
  .catch(next)
});

// router.post('/', users.create);

router.get('/', users.findAll);

router.get('/:id', users.findOne);

router.put('/:id', users.update);

router.delete('/:id', users.delete);

module.exports = router;