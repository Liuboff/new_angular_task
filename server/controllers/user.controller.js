const fileService = require('../services/fileServices');
const db = require('../models');
const User = db.users;

// exports.create = (req, res) => {
//   if (!req.body.email) {
//     res.status(400).send({ message: 'User can not be without email!' });
//     return;
//   }
//   const user = new User({
//     email: req.body.email,
//     password: req.body.password,
//     name: req.body.name,
//     age: req.body.age
//   });

//   user.save(user)
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || 'Some error occurred while creating the User.'
//       });
//     });
// };

exports.create = (body, files) => {
  return new Promise((resolve, reject) => {
    if (!body.email) {
      reject('User can not be without email!');
    }
    const fileName = fileService.saveFile(files.avatar);

    const user = new User({
      email: body.email,
      password: body.password,
      name: body.name,
      age: body.age,
      friends: body.friends,
      games: body.games,
      phone: body.phone,
      token: body.token,
      avatar: fileName
    });

    user.save(user)
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err.message || 'Some error occurred while creating the User.');
      });
  })
}

exports.findAll = async (req, res) => {
  await User.find()
    .then(users => {
      res.send(users);
      // res.json(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving users.'
      });
    });
};

// Find a single user with an id
exports.findOne = async (req, res) => {
  const id = req.params.id;
  await User.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: `Not found User with id ${id}` });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: `Error retrieving User with id=${id}` });
    });
};

// Update a user by the id in the request
exports.update = async (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: 'Data to update can not be empty!'
    });
  }
  const id = req.params.id;
  await User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update User with id=${id}. Maybe User was not found!`
        });
      } else res.send({ message: 'User was updated successfully.' });
    })
    .catch(err => {
      res.status(500).send({
        message: `Error updating User with id=${id}`
      });
    });
};

// Delete a user with the specified id in the request
exports.delete = async (req, res) => {
  const id = req.params.id;
  await User.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      } else {
        res.send({
          message: 'User was deleted successfully!'
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Could not delete User with id=${id}`
      });
    });
};

// Find a single user with an email
// exports.findOneByEmail = (req, res) => {
//   const email = req.query.email;
//   let condition = email ? { email: { $regex: new RegExp(email), $options: 'i' } } : {};
//   User.findOneByEmail(condition)
//     .then(data => {
//       if (!data)
//         res.status(404).send({ message: "Not found User with email " + email });
//       else res.send(data);
//     })
//     .catch(err => {
//       res
//         .status(500)
//         .send({ message: "Error retrieving User with email=" + email });
//     });
// };